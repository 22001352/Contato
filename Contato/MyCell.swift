//
//  MyCell.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class MyCell: UITableViewCell {
    @IBOutlet var nome: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var endereco: UILabel!
    @IBOutlet var numero: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
