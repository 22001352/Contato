//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome:"Contato 1", numero: "31 98989-0000", email: "contato1@email.com.br", endereco: "Rua do Cristal 11"))
        listaDeContatos.append(Contato(nome:"Contato 2", numero: "31 98989-1111", email: "contato1@email.com.br", endereco: "Rua do Cristal 22"))
        listaDeContatos.append(Contato(nome:"Contato 3", numero: "31 98989-2222", email: "contato1@email.com.br", endereco: "Rua do Cristal 33"))
    }


}
